package com.kalebdodd.stacker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by balke on 8/30/2017.
 */

public class GameWinMenu implements GameObject {
    private Paint boxes;
    private Paint text;
    private Rect menuBox;
    private Rect winnerRect;
    private ImageButton menuButton;
    private Bitmap image;
    private Bitmap winnerPic;


    public GameWinMenu(Context context){
        this.boxes = new Paint();
        this.boxes.setColor(Color.BLUE);
        this.text = new Paint();
        this.text.setColor(Color.WHITE);
        this.text.setTextSize(70);
        this.text.setTextAlign(Paint.Align.CENTER);
        this.image = BitmapFactory.decodeResource(context.getApplicationContext().getResources(), com.kalebdodd.stacker.R.drawable.menu);
        this.winnerPic = BitmapFactory.decodeResource(context.getApplicationContext().getResources(), com.kalebdodd.stacker.R.drawable.winner);
        this.winnerRect = new Rect((int)GamePanel.getXPos(0.05),(int)GamePanel.getYPos(0.10),
                (int)GamePanel.getXPos(0.95),(int)GamePanel.getYPos(0.45));

        this.menuBox = new Rect((int)GamePanel.getXPos(0.35),(int)GamePanel.getYPos(0.70),
                (int)GamePanel.getXPos(0.65),(int)GamePanel.getYPos(0.76));
        this.menuButton = new ImageButton(this.menuBox,this.image,this.boxes);


    }

    @Override
    public void draw(Canvas canvas) {
        this.menuButton.draw(canvas);
        canvas.drawBitmap(this.winnerPic,null,this.winnerRect,this.boxes);
        //canvas.drawText("You Won!",(int)GamePanel.getXPos(0.5),(int)GamePanel.getYPos(0.2),this.text);

    }

    @Override
    public void update() {

    }

    public boolean isMenuPressed(int x,int y){
        return this.menuButton.isPressed(x,y);
    }
}
