package com.kalebdodd.stacker;

import android.graphics.Canvas;

/**
 * Created by Kaleb on 7/31/2017.
 */

public interface GameObject {
    public void draw(Canvas canvas);
    public void update();



}
