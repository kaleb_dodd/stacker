package com.kalebdodd.stacker;

import android.graphics.Canvas;
import android.view.SurfaceHolder;



/**
 * Created by Kaleb on 7/30/2017.
 */

public class MainThread extends Thread {
    private long startOfCycleTime;
    private double timeSinceLastCycle;
    private double fpsTarget = 60;
    private double currentFPS;


    private SurfaceHolder surfaceHolder;
    private  GamePanel gamePanel;
    private boolean running;
    public static Canvas canvas;

    public MainThread(SurfaceHolder surfaceHolder, GamePanel gamePanel){
        super();
        this.surfaceHolder = surfaceHolder;
        this.gamePanel = gamePanel;
    }

    public void run(){


        while(this.running){
            this.startOfCycleTime = System.currentTimeMillis();
            canvas = null;
            try{
                canvas = this.surfaceHolder.lockCanvas();
                synchronized (surfaceHolder){
                    this.gamePanel.update();
                    this.gamePanel.draw(canvas,this.currentFPS);
                }
            }catch (Exception e){
                e.printStackTrace();
            } finally {
                if(canvas != null){
                    try{
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
            this.timeSinceLastCycle = System.currentTimeMillis() - this.startOfCycleTime;
            double pauseTime = (1000.0 / this.fpsTarget) - this.timeSinceLastCycle;
            try{
                if(pauseTime > 0){
                    this.currentFPS = 1000.0 / (this.timeSinceLastCycle + pauseTime);
                    this.sleep((long)pauseTime);
                    this.timeSinceLastCycle += pauseTime;
                }else{
                    this.currentFPS = 1000.0 / this.timeSinceLastCycle;
                }
            } catch (Exception e){
                e.printStackTrace();
            }


        }
    }

    public void setRunning(boolean running){
        this.running = running;
    }

    public double getFPS(){
        return this.currentFPS;
    }

    public void setFpsTarget(double fps){
        this.fpsTarget = fps;
    }
}
