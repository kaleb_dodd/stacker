package com.kalebdodd.stacker;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

/**
 * Created by Kaleb on 7/30/2017.
 */

public class GamePanel extends SurfaceView implements SurfaceHolder.Callback {
    private MainThread thread;
    private ArrayList<RectPlayer> rects = new ArrayList<>();
    private ArrayList<FallingBlock> fallingBlocks = new ArrayList<>();

    private Background background;
    private MainMenu mainMenu;
    private GameOverMenu gameOverMenu;
    private PauseMenu pauseMenu;
    private GameWinMenu winMenu;
    private LevelMenu levelMenu;
    private FileIO fileIO;

    private static int width;
    private static int levelsUnlocked;
    private static int height;
    private static double xOffset;
    private static double yOffset;
    private static double lineThickness;
    private static double xLength;
    private static double yLength;
    private static double squareLength;
    private boolean firstCycle = true;
    private int score = 0;
    private int speed = 150;
    private GameState gameState = GameState.MAIN_MENU;
    private static GameMode gameMode;
    private boolean finishedFalling = true;
    private boolean gameOverFlag = false;
    private Paint text;


    public GamePanel (Context context){
        super(context);
        getHolder().addCallback(this);

        this.thread = new MainThread(getHolder(),this);
        setFocusable(true);
    }

    public enum GameState {
        MAIN_MENU,
        LEVEL_MENU,
        PAUSE_MENU,
        PLAYING,
        GAME_OVER,
        RESTART,
        GAME_WON,
    }

    public enum GameMode {
        STANDARD_EASY,
        STANDARD_MED,
        STANDARD_HARD,
        ENDLESS_EASY,

    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if(this.firstCycle) {
            this.width = this.getWidth();
            this.height = this.getHeight();
            this.yOffset = this.getYPos(0.09);
            this.xOffset = this.getXPos(0.1);
            this.lineThickness = Math.round(this.getXPos(0.015));

            this.squareLength = Math.round(((this.getXPos(1.0) - this.xOffset - this.xOffset) - (this.lineThickness * 8)) / 7.0);
            this.xLength = xOffset + (8 * lineThickness) + (7 * squareLength);
            this.yLength = ((this.lineThickness + this.squareLength) * 11.0) + this.yOffset;
            //this.rects.add(new RectPlayer(this.getContext(),rand(0,3), 10, (int) Math.round(squareLength/2.0), 3, this.speed,rand(0,1)==1, Color.rgb(255, 0, 255)));

            this.background = new Background();
            this.levelMenu = new LevelMenu(this.getContext());
            this.mainMenu = new MainMenu(this.getContext());
            this.gameOverMenu = new GameOverMenu(this.getContext());
            this.pauseMenu = new PauseMenu(this.getContext());
            this.winMenu = new GameWinMenu(this.getContext());
            this.text = new Paint();
            this.text.setTextSize(64);
            this.text.setAntiAlias(true);
            this.text.setColor(Color.BLUE);
            this.text.setTextAlign(Paint.Align.CENTER);
            this.fileIO = FileIO.getInstance(this.getContext());

            levelsUnlocked = 0;


            String input = this.fileIO.read();
            //System.out.println("INTPUT:" + input);
            try {
                levelsUnlocked = (Integer.parseInt(input));
            }catch (NumberFormatException e){
                e.printStackTrace();
            }

            //System.out.println("PRINTING");
            //System.out.println(levelsUnlocked);


            this.thread = new MainThread(getHolder(), this);
            this.thread.setRunning(true);
            this.thread.start();
            this.firstCycle = false;
        }else{
            this.thread = new MainThread(getHolder(), this);
            this.thread.setRunning(true);
            this.thread.start();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        boolean retry = true;
        while(retry){
            try{
                if(this.gameState == GameState.PLAYING){
                    if(!this.gameOverFlag){
                        this.gameState = GameState.PAUSE_MENU;
                    }else{
                        this.gameState = GameState.GAME_OVER;
                    }
                }
                this.fileIO.write(this.levelsUnlocked);
                //this.fileIO.write("TEST");
                this.fileIO.close();
                this.thread.setRunning(false);
                this.thread.join();
            } catch(Exception e){
                e.printStackTrace();
            }
            retry = false;

        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        if (this.gameState == GameState.PLAYING) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                int size;
                if (this.rects.size() > 1) {
                    size = this.rects.get(this.rects.size() - 1).onTouch(this.rects.get(this.rects.size() - 2));
                } else {
                    size = 3;
                    this.rects.get(0).onTouch();
                }

                if(size != 3 && size != this.rects.get(this.rects.size()-2).getSize()){ // we are getting smaller
                    this.fallingBlocks.add(new FallingBlock(this.getContext(),
                            this.rects.get(this.rects.size()-1).getFallingBlockPos().x,
                            this.rects.get(this.rects.size()-1).getFallingBlockPos().y,(int) Math.round(squareLength/2.0),
                            this.rects.get(this.rects.size()-1).getFallingBlockSize(),Color.rgb(255, 0, 255)));
                }


                if(!this.fallingBlocks.isEmpty()){
                    this.finishedFalling = this.fallingBlocks.get(this.fallingBlocks.size()-1).isFinished();
                }else{
                    this.finishedFalling = true;
                }

                if (size == 0) {
                    this.gameOverFlag = true;
                    this.rects.remove(this.rects.size()-1);
                    waitUntilFinishedFalling();
                    this.gameState = GameState.GAME_OVER;
                    return true;
                }
                int bounceCount = this.rects.get(this.rects.size()-1).getBounceCount();
                int scoreToAdd = (int)((size*100) * (1.0-(bounceCount/10.0)));
                int finalScore = Math.max(scoreToAdd,size*50);

                this.score += finalScore;

                if(gameMode == GameMode.STANDARD_EASY){
                    this.speed -= 13;
                    if (this.speed < 80) {
                        this.speed = 80;
                    }
                }else if(gameMode == GameMode.STANDARD_MED){
                    this.speed -= 14;
                    if (this.speed < 70) {
                        this.speed = 70;
                    }
                }else if(gameMode == GameMode.STANDARD_HARD){
                    this.speed -= 14;
                    if (this.speed < 55) {
                        this.speed = 55;
                    }
                }else if(gameMode == GameMode.ENDLESS_EASY){
                    this.score *= 1.1;
                    this.speed *= 0.91;
                    if (this.speed < 30) {
                        this.speed = 30;
                    }
                }

                if(gameMode != GameMode.ENDLESS_EASY) {
                    if (this.rects.get(this.rects.size() - 1).getYPos() == 0) {
                        waitUntilFinishedFalling();
                        pause(300);
                        if(this.levelsUnlocked < 3){
                            this.levelsUnlocked++;
                        }

                        this.gameState = GameState.GAME_WON;
                    } else {
                        waitUntilFinishedFalling();
                        this.rects.add(new RectPlayer(this.getContext(),this.gameMode ,rand(0, 3), this.rects.get(this.rects.size() - 1).getYPos() - 1, (int) Math.round(squareLength / 2.0), size, this.speed, rand(0, 1) == 1, Color.rgb(255, 0, 255)));
                    }
                }else{
                    waitUntilFinishedFalling();
                    if(this.rects.get(this.rects.size()-1).getYPos() < 1){
                        pause(200);
                        for(int i = this.rects.size()-1; i > 0; i--){
                            this.rects.get(i).setY(this.rects.get(i).getYPos()+1);
                        }
                        this.rects.remove(0);

                    }

                    this.rects.add(new RectPlayer(this.getContext(), this.gameMode,rand(0, 3), this.rects.get(this.rects.size() - 1).getYPos() - 1, (int) Math.round(squareLength / 2.0), size, this.speed, rand(0, 1) == 1, Color.rgb(255, 0, 255)));

                }

            }
        }else if(this.gameState == GameState.MAIN_MENU){
            if(event.getAction() == MotionEvent.ACTION_DOWN){
                if(this.mainMenu.isPlayPressed((int)event.getX(),(int)event.getY())){
                    this.gameState = GameState.LEVEL_MENU;
                }
            }
        }else if(this.gameState == GameState.GAME_OVER){
            if(event.getAction() == MotionEvent.ACTION_DOWN){
                if(this.gameOverMenu.isResetPressed((int)event.getX(),(int)event.getY())){
                    this.gameState = GameState.RESTART;
                }else if(this.gameOverMenu.isMainMenuPressed((int)event.getX(),(int)event.getY())){
                    this.gameState = GameState.MAIN_MENU;
                }
            }
        }else if(this.gameState == GameState.PAUSE_MENU){
            if(event.getAction() == MotionEvent.ACTION_DOWN){
                if(this.pauseMenu.isPlayPressed((int)event.getX(),(int)event.getY())){
                    this.gameState = GameState.PLAYING;
                }
            }
        }else if(this.gameState == GameState.GAME_WON){
            if(event.getAction() == MotionEvent.ACTION_DOWN){
                if(this.winMenu.isMenuPressed((int)event.getX(),(int)event.getY())){
                    this.gameState = GameState.MAIN_MENU;
                }
            }
        }else if(this.gameState == GameState.LEVEL_MENU){
            if(event.getAction() == MotionEvent.ACTION_DOWN){
                if(this.levelMenu.isEasyPressed((int)event.getX(),(int)event.getY())){
                    this.gameMode = GameMode.STANDARD_EASY;
                    this.gameState = GameState.RESTART;
                }else if(this.levelMenu.isMedPressed((int)event.getX(),(int)event.getY())){
                    if(this.levelsUnlocked >= 1){
                        this.gameMode = GameMode.STANDARD_MED;
                        this.gameState = GameState.RESTART;
                    }

                }else if(this.levelMenu.isHardPressed((int)event.getX(),(int)event.getY())){
                    if(this.levelsUnlocked >= 2){
                        this.gameMode = GameMode.STANDARD_HARD;
                        this.gameState = GameState.RESTART;
                    }

                }else if(this.levelMenu.isEndlessPressed((int)event.getX(),(int)event.getY())){
                    if(this.levelsUnlocked >= 3){
                        this.gameMode = GameMode.ENDLESS_EASY;
                        this.gameState = GameState.RESTART;
                    }

                }
            }
        }

        return true;
    }

    public void update() {

        if (this.gameState == GameState.PLAYING){
            for (int i = 0; i < this.rects.size(); i++) {
                this.rects.get(i).update();
            }

            for(int i = 0; i< this.fallingBlocks.size(); i++){
                this.fallingBlocks.get(i).update();
            }

        }else if(this.gameState == GameState.RESTART){
            this.rects.clear();
            this.fallingBlocks.clear();
            this.gameOverFlag = false;
            this.score = 0;
            if(gameMode == GameMode.STANDARD_EASY){
                this.speed = 200;
            }else if(gameMode == GameMode.STANDARD_MED){
                this.speed = 150;
            }else if(gameMode == GameMode.STANDARD_HARD){
                this.speed = 145;
            }else if(gameMode == GameMode.ENDLESS_EASY){
                this.speed = 250;
            }

            this.rects.add(new RectPlayer(this.getContext(),this.gameMode,rand(0,3), 10, (int) Math.round(squareLength/2.0), 3, this.speed,rand(0,1)==1, Color.rgb(255, 0, 255)));
            this.gameState = GameState.PLAYING;
        }
    }


    public void draw(Canvas canvas,double fps){
        super.draw(canvas);
        if(this.gameState != GameState.PLAYING){
            canvas.drawColor(Color.BLACK);
        }else{
            canvas.drawColor(Color.BLACK);
        }

        if(this.gameState == GameState.PLAYING) {
            this.background.draw(canvas);
            this.drawScore(canvas);
            for (int i = 0; i < this.rects.size(); i++) {
                this.rects.get(i).draw(canvas);
            }
            for (int i = 0; i < this.fallingBlocks.size(); i++) {
                this.fallingBlocks.get(i).draw(canvas);
            }
        }else if(this.gameState == GameState.MAIN_MENU){
            this.mainMenu.draw(canvas);
        }else if(this.gameState == GameState.GAME_OVER){
            this.gameOverMenu.draw(canvas);
            this.drawScore(canvas);
        }else if(this.gameState == GameState.PAUSE_MENU){
            this.pauseMenu.draw(canvas);
        }else if(this.gameState == GameState.GAME_WON){
            this.winMenu.draw(canvas);
            this.drawScore(canvas);
        }else if(this.gameState == GameState.LEVEL_MENU){
            this.levelMenu.draw(canvas);
        }

    }

    public void drawScore (Canvas canvas){
        canvas.drawText("SCORE: " + this.score,(float)GamePanel.getXPos(0.35),(float)(GamePanel.yOffset - GamePanel.getYPos(0.035)),this.text);
    }

    public static int getUnlockedLevels(){
        return levelsUnlocked;
    }

    public static GameMode getGameMode(){
        return gameMode;
    }

    public static double getYOffset(){
        return  yOffset;
    }

    public static double getXOffset(){
        return  xOffset;
    }

    public static double getLineThickness(){
        return  lineThickness;
    }

    public static double getYLength(){
        return  yLength;
    }

    public static double getXLength(){
        return  xLength;
    }

    public static double getXPos(double percent){
       return (percent * width);
    }

    public static double getYPos(double percent){
        return (percent * height);
    }

    public static double getSquareLength(){
        return squareLength;
    }

    public static Point getCoord(int x, int y){ // input is grid pos output is pixel pos
        return new Point((int)(xOffset+(squareLength/2.0) + lineThickness+((x*(lineThickness+(squareLength))))),
                (int)(yOffset+(squareLength/2.0)+lineThickness+((y*(lineThickness+(squareLength))))));
    }

    public static Point getCoord(Point p){
        return new Point((int)(xOffset+(squareLength/2.0) + lineThickness+((p.x*(lineThickness+(squareLength))))),
                (int)(yOffset+(squareLength/2.0)+lineThickness+((p.y*(lineThickness+(squareLength))))));
    }

    public static int rand(double min, double max){
        return  (int)Math.round((Math.random()*(max-min))+min);
    }

    public static void pause(long pauseTime){
        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < pauseTime){

        }

    }

    public void waitUntilFinishedFalling(){
        while (!this.finishedFalling){
            this.finishedFalling = this.fallingBlocks.get(this.fallingBlocks.size()-1).isFinished();
        }
        this.fallingBlocks.clear();
    }







}
