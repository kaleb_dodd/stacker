package com.kalebdodd.stacker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by balke on 8/30/2017.
 */

public class PauseMenu implements GameObject {
    private Paint boxes;
    private Paint text;
    private Rect playBox;
    private ImageButton playButton;
    private Bitmap image;


    public PauseMenu(Context context){
        this.boxes = new Paint();
        this.boxes.setColor(Color.BLUE);
        this.text = new Paint();
        this.text.setColor(Color.WHITE);
        this.text.setTextSize(60);
        this.text.setTextAlign(Paint.Align.CENTER);

        this.image = BitmapFactory.decodeResource(context.getApplicationContext().getResources(), com.kalebdodd.stacker.R.drawable.resume);

        this.playBox = new Rect((int)GamePanel.getXPos(0.35),(int)GamePanel.getYPos(0.45),
                (int)GamePanel.getXPos(0.65),(int)GamePanel.getYPos(0.51));
        this.playButton = new ImageButton(this.playBox,this.image,this.boxes);

    }

    @Override
    public void draw(Canvas canvas) {
        this.playButton.draw(canvas);

    }

    @Override
    public void update() {

    }

    public boolean isPlayPressed(int x,int y){
        return this.playButton.isPressed(x,y);
    }
}
