package com.kalebdodd.stacker;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by balke on 8/30/2017.
 */

public class Button {
    protected Rect rect;
    private String text;
    protected Paint boxP;
    private Paint textP;

    public Button(Rect rect, Paint boxP, Paint textP,String text){
        this.rect = rect;
        this.text = text;
        this.boxP = boxP;
        this.textP = textP;
    }

    public Button(Rect rect, Paint overlay){
        this.rect = rect;
        this.boxP = overlay;
    }

    public void draw(Canvas canvas){
        canvas.drawRect(this.rect,boxP);
        canvas.drawText(this.text,this.rect.centerX(),this.rect.centerY() + this.textP.getTextSize()/2,this.textP);
    }

    public boolean isPressed(int x, int y){
        if(x >= this.rect.left && x <= this.rect.right){
            if(y >= this.rect.top && y <= this.rect.bottom){
                return true;
            }
        }
        return  false;
    }

}
