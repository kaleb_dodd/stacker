package com.kalebdodd.stacker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

import java.util.ArrayList;

/**
 * Created by Kaleb on 7/31/2017.
 */

public class RectPlayer implements  GameObject {

    private ArrayList<Rect> rects = new ArrayList<>();
    private int colour;
    private int xPos;
    private int yPos;
    private int size;
    private boolean goingRight;
    private int spacing;
    private boolean moving = true;
    private long speed;
    private long lastTimeMoved;
    private Bitmap squarePic;

    private Point fallingBlockPos;
    private int fallingBlockSize;
    private Paint filter;
    private int bounceCount = 0;

    public RectPlayer(Context context,GamePanel.GameMode gameMode,int x, int y, int spacing, int size, int speed, boolean goingRight, int colour){
        this.colour = colour;
        this.filter = new Paint();
        this.size = size;
        this.goingRight = goingRight;
        this.spacing = spacing;
        this.xPos = x;
        this.yPos = y;
        this.speed = speed;
        this.lastTimeMoved = System.currentTimeMillis();
        this.squarePic = BitmapFactory.decodeResource(context.getApplicationContext().getResources(), com.kalebdodd.stacker.R.drawable.block);
        int offset = (int)(11 * (10-this.yPos));
        if(gameMode == GamePanel.GameMode.STANDARD_EASY) {
            this.filter.setColorFilter(new LightingColorFilter(Color.rgb(60, 165, 60), Color.rgb(offset, 210, offset)));
        }else if(gameMode == GamePanel.GameMode.STANDARD_MED){
            this.filter.setColorFilter(new LightingColorFilter(Color.rgb(165, 165, 165), Color.rgb(200, offset, offset)));
        }else if(gameMode == GamePanel.GameMode.STANDARD_HARD){
            this.filter.setColorFilter(new LightingColorFilter(Color.rgb(165, 70, 70), Color.rgb(210, offset, offset)));
        }else if(gameMode == GamePanel.GameMode.ENDLESS_EASY){
            this.filter.setColorFilter(new LightingColorFilter(Color.rgb(120, 255, 10), Color.rgb(100+offset, offset+50, offset)));
        }

        x = GamePanel.getCoord(x,y).x;
        y = GamePanel.getCoord(x,y).y;

        for(int i = 0; i < size; i++){
            x += i*(GamePanel.getLineThickness()+GamePanel.getSquareLength());
            this.rects.add(new Rect(x-spacing,y-spacing,x+spacing,y+spacing));
        }

    }



   public void draw(Canvas canvas) {
        for(int i = 0; i < this.size; i++){
            canvas.drawBitmap(this.squarePic,null,this.rects.get(i),this.filter);
            //canvas.drawRect(this.rects.get(i),paint);
        }

    }

    @Override
    public void update() {

        if(this.moving) {
            long dt = System.currentTimeMillis() - this.lastTimeMoved;
            if(dt >= speed) {
                if (this.goingRight) {
                    if ((this.xPos + this.size - 1) < 6) { // not hitting the edge
                        this.xPos++;
                    } else {
                        this.xPos--;
                        this.goingRight = false;
                        this.bounceCount++;
                    }
                } else { // going left
                    if (this.xPos > 0) {
                        this.xPos--;
                    } else {
                        this.xPos++;
                        this.goingRight = true;
                        this.bounceCount++;
                    }
                }
                this.lastTimeMoved = System.currentTimeMillis();
            }
        }
        for (int i = 0; i < this.size; i++) {
            int x = GamePanel.getCoord(this.xPos, this.yPos).x;
            int y = GamePanel.getCoord(this.xPos, this.yPos).y;
            this.rects.get(i).set((int) (x - spacing + (i * (GamePanel.getLineThickness()
                            + GamePanel.getSquareLength()))), y - spacing,
                    (int) (x + spacing + (i * (GamePanel.getLineThickness() +
                            GamePanel.getSquareLength()))), y + spacing);
        }
    }



    public void onTouch(){
        this.moving = false;
    }

    public int onTouch(RectPlayer last){
        this.moving = false;
        if(this.xPos == last.getXPos()){ // perfectly lined up
            return  this.size;
        }
        if(this.size == 3){
            if(this.xPos == last.getXPos()-2){ // only the right most is overlapping
                this.fallingBlockPos = new Point(this.xPos,this.yPos);
                this.fallingBlockSize = 2;
                this.xPos = last.getXPos();
                this.size = 1;
            }else if(this.xPos == last.getXPos()-1){ // right two overlapping
                this.fallingBlockPos = new Point(this.xPos,this.yPos);
                this.fallingBlockSize = 1;
                this.xPos = last.getXPos();
                this.size = 2;
            }else if(this.xPos == last.getXPos()+1){ // left two overlapping
                this.fallingBlockPos = new Point(this.xPos+2,this.yPos);
                this.fallingBlockSize = 1;
                this.size =2;
            }else if(this.xPos == last.getXPos()+2){ // left most is overlapping
                this.fallingBlockPos = new Point(this.xPos+1,this.yPos);
                this.fallingBlockSize = 2;
                this.size = 1;
            }else{ // not lined up
                this.fallingBlockPos = new Point(this.xPos,this.yPos);
                this.fallingBlockSize = 3;
                return 0;
            }
        }else if(this.size == 2){
            if(last.getSize() == 3){
                if(this.xPos == last.getXPos()-1){ // only right one is overlapping
                    this.fallingBlockPos = new Point(this.xPos,this.yPos);
                    this.fallingBlockSize = 1;
                    this.xPos = last.getXPos();
                    this.size = 1;
                }else if(this.xPos == last.getXPos()+1){ // both are overlapping
                    this.size = 2;
                }else if(this.xPos == last.getXPos()+2){ // only left one is overlapping
                    this.fallingBlockPos = new Point(this.xPos+1,this.yPos);
                    this.fallingBlockSize = 1;
                    this.size = 1;
                }else{ // not lined up
                    this.fallingBlockPos = new Point(this.xPos,this.yPos);
                    this.fallingBlockSize = 2;
                    return 0;
                }
            }else{ // last is size 2
                if(this.xPos == last.getXPos()-1){ // right lines up
                    this.fallingBlockPos = new Point(this.xPos,this.yPos);
                    this.fallingBlockSize = 1;
                    this.xPos = last.getXPos();
                    this.size = 1;
                }else if(this.xPos == last.getXPos()+1){ // left lines up
                    this.fallingBlockPos = new Point(this.xPos+1,this.yPos);
                    this.fallingBlockSize = 1;
                    this.size = 1;
                }else{
                    this.fallingBlockPos = new Point(this.xPos,this.yPos);
                    this.fallingBlockSize = 2;
                    return 0;
                }
            }
        }else if(this.size == 1){
            if(last.getSize() == 3){ // last size is 3
                if(this.xPos == last.getXPos()+1){
                    return  1;
                }else if(this.xPos == last.getXPos()+2){
                    return  1;
                }else{
                    this.fallingBlockPos = new Point(this.xPos,this.yPos);
                    this.fallingBlockSize = 1;
                    return  0;
                }
            }else if(last.getSize() ==2){ // size is 2
                if(this.xPos == last.getXPos()+1){
                    return  1;
                }else{
                    this.fallingBlockPos = new Point(this.xPos,this.yPos);
                    this.fallingBlockSize = 1;
                    return  0;
                }
            }else{
                this.fallingBlockPos = new Point(this.xPos,this.yPos);
                this.fallingBlockSize = 1;
                return 0;
            }
        }

        return this.size;
    }

    public void setY(int y){
        this.yPos = y;
    }

    public int getXPos(){
        return this.xPos;
    }

    public int getYPos(){
        return this.yPos;
    }

    public int getSize(){
        return this.size;
    }

    public Point getFallingBlockPos(){
        return this.fallingBlockPos;
    }

    public int getFallingBlockSize(){
        return this.fallingBlockSize;
    }

    public int getBounceCount(){
        return this.bounceCount;
    }
}
