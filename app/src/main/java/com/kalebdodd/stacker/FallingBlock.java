package com.kalebdodd.stacker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;

import java.util.ArrayList;

/**
 * Created by Kaleb on 7/31/2017.
 */

public class FallingBlock implements  GameObject {

    private ArrayList<Rect> rects = new ArrayList<>();
    private int colour;
    private int xPos;
    private int yPos;
    private int size;
    private int spacing;
    private long speed = 100;
    private long lastTimeMoved;
    private Bitmap squarePic;
    private boolean finished = false;
    private Paint filter;
    private int darkness = 1;

    public FallingBlock(Context context, int x, int y, int spacing, int size, int colour){
        this.colour = colour;
        this.size = size;
        this.spacing = spacing;
        this.xPos = x;
        this.yPos = y;
        this.filter = new Paint();
        this.lastTimeMoved = System.currentTimeMillis();
        this.squarePic = BitmapFactory.decodeResource(context.getApplicationContext().getResources(), com.kalebdodd.stacker.R.drawable.block);


        x = GamePanel.getCoord(x,y).x;
        y = GamePanel.getCoord(x,y).y;

        for(int i = 0; i < size; i++){
            x += i*(GamePanel.getLineThickness()+GamePanel.getSquareLength());
            this.rects.add(new Rect(x-spacing,y-spacing,x+spacing,y+spacing));
        }

    }

    @Override
    public void draw(Canvas canvas) {
        if(!this.finished) {
            for (int i = 0; i < this.size; i++) {
                canvas.drawBitmap(this.squarePic, null, this.rects.get(i), this.filter);
                //canvas.drawRect(this.rects.get(i),paint);
            }
        }

    }

    @Override
    public void update() {

        if(this.yPos < 10) {
            long dt = System.currentTimeMillis() - this.lastTimeMoved;
            if(dt >= speed) {
                this.yPos++;
                this.filter.setColorFilter(new LightingColorFilter(Color.rgb(180,180,180),0));
                this.lastTimeMoved = System.currentTimeMillis();
            }
        }else{
            GamePanel.pause(250);
            this.finished = true;
        }
        for (int i = 0; i < this.size; i++) {
            int x = GamePanel.getCoord(this.xPos, this.yPos).x;
            int y = GamePanel.getCoord(this.xPos, this.yPos).y;
            this.rects.get(i).set((int) (x - spacing + (i * (GamePanel.getLineThickness()
                            + GamePanel.getSquareLength()))), y - spacing,
                    (int) (x + spacing + (i * (GamePanel.getLineThickness() +
                            GamePanel.getSquareLength()))), y + spacing);
        }
    }






    public int getXPos(){
        return this.xPos;
    }

    public int getYPos(){
        return this.yPos;
    }

    public int getSize(){
        return this.size;
    }

    public boolean isFinished(){
        return this.finished;
    }
}
