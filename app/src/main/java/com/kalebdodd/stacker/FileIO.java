package com.kalebdodd.stacker;

import android.content.Context;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


/**
 * Created by Kaleb on 9/19/2017.
 */

public class FileIO {

    private String FileName = "test.txt";
    private BufferedWriter writer;
    private BufferedReader reader;
    private FileOutputStream outputStream;
    private FileInputStream inputStream;
    private Context context;
    private static FileIO instance;

    public static FileIO getInstance(Context context){
        if(instance == null){
            instance = new FileIO(context);
        }
        return instance;
    }

    private FileIO(Context context){
        this.context = context;

    }



    public void write(int input){
        try {
            this.outputStream = this.context.openFileOutput(this.FileName, Context.MODE_PRIVATE);
            this.writer = new BufferedWriter(new OutputStreamWriter(this.outputStream));
            this.writer.write(""+input);
            this.writer.newLine();
            //this.writer.flush();
            System.out.println("FINISHED WRITING");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close(){
        try {
            this.writer.close();
            this.outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String read(){
        try {
            this.inputStream = this.context.openFileInput(this.FileName);
            this.reader = new BufferedReader(new InputStreamReader(this.inputStream));
            StringBuffer stringBuffer = new StringBuffer();
            String temp;
            while ((temp=reader.readLine()) != null){
                //System.out.println("readLine");
                stringBuffer.append(temp);
            }
            String output = stringBuffer.toString();
            //System.out.println("FINISHED READING");
            //System.out.println(output);
            //System.out.println("SOMETHING IN BETWEEN THESE PRINTS");
            return output;
        }catch(FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";

    }






}
