package com.kalebdodd.stacker;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by balke on 8/30/2017.
 */

public class LevelMenu implements GameObject {
    private Paint boxes;
    private Paint locked;
    private Paint text;

    private Rect easyBox;
    private Rect medBox;
    private Rect hardBox;
    private Rect endlessBox;


    private Button medButtonLocked;
    private Button hardButtonLocked;
    private Button endlessButtonLocked;

    private Button easyButton;
    private Button medButton;
    private Button hardButton;
    private Button endlessButton;





    public LevelMenu(Context context){
        this.boxes = new Paint();
        this.boxes.setColor(Color.BLUE);
        this.locked = new Paint();
        this.locked.setColor(Color.GRAY);
        this.text = new Paint();
        this.text.setColor(Color.rgb(255,165,0));
        this.text.setTextSize(64);
        this.text.setAntiAlias(true);
        this.text.setTextAlign(Paint.Align.CENTER);

        this.easyBox = new Rect((int)GamePanel.getXPos(0.26),(int)GamePanel.getYPos(0.25),
                (int)GamePanel.getXPos(0.74),(int)GamePanel.getYPos(0.33));
        this.easyButton = new Button(this.easyBox,this.boxes,this.text,"EASY");

        this.medBox = new Rect((int)GamePanel.getXPos(0.26),(int)GamePanel.getYPos(0.43),
                (int)GamePanel.getXPos(0.74),(int)GamePanel.getYPos(0.51));
        this.medButton = new Button(this.medBox,this.boxes,this.text,"NORMAL");
        this.medButtonLocked = new Button(this.medBox,this.locked,this.text,"LOCKED");

        this.hardBox = new Rect((int)GamePanel.getXPos(0.26),(int)GamePanel.getYPos(0.61),
                (int)GamePanel.getXPos(0.74),(int)GamePanel.getYPos(0.69));
        this.hardButton = new Button(this.hardBox,this.boxes,this.text,"HARD");
        this.hardButtonLocked =  new Button(this.hardBox,this.locked,this.text,"LOCKED");

        this.endlessBox = new Rect((int)GamePanel.getXPos(0.26),(int)GamePanel.getYPos(0.79),
                (int)GamePanel.getXPos(0.74),(int)GamePanel.getYPos(0.87));
        this.endlessButton = new Button(this.endlessBox,this.boxes,this.text,"ENDLESS");
        this.endlessButtonLocked = new Button(this.endlessBox,this.locked,this.text,"LOCKED");
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawText("SELECT MODE",(int)GamePanel.getXPos(0.5),(int)GamePanel.getYPos(0.15),this.text);
        this.easyButton.draw(canvas);
        if(GamePanel.getUnlockedLevels() >= 3){
            this.endlessButton.draw(canvas);
        }else{
            this.endlessButtonLocked.draw(canvas);
        }
        if(GamePanel.getUnlockedLevels() >= 2){
            this.hardButton.draw(canvas);
        }else{
            this.hardButtonLocked.draw(canvas);
        }
        if(GamePanel.getUnlockedLevels() >= 1){
            this.medButton.draw(canvas);
        }else{
            this.medButtonLocked.draw(canvas);
        }
    }

    @Override
    public void update() {

    }

    public boolean isEasyPressed(int x,int y){
        return this.easyButton.isPressed(x,y);
    }

    public boolean isMedPressed(int x,int y){
        return this.medButton.isPressed(x,y);
    }

    public boolean isHardPressed(int x,int y){
        return this.hardButton.isPressed(x,y);
    }

    public boolean isEndlessPressed(int x,int y){
        return this.endlessButton.isPressed(x,y);
    }

}
