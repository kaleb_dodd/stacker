package com.kalebdodd.stacker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by balke on 8/30/2017.
 */

public class MainMenu implements GameObject {
    private Paint boxes;
    private Paint text;
    private Rect playBox;
    private ImageButton playButton;
    private Bitmap image;


    public  MainMenu(Context context){
        this.boxes = new Paint();
        this.boxes.setColor(Color.BLUE);
        this.image = BitmapFactory.decodeResource(context.getApplicationContext().getResources(), com.kalebdodd.stacker.R.drawable.start1);
        this.text = new Paint();
        this.text.setColor(Color.rgb(255,32,255));
        this.text.setTextSize(70);
        this.text.setTextAlign(Paint.Align.CENTER);
        this.text.setAntiAlias(true);
        this.playBox = new Rect((int)GamePanel.getXPos(0.26),(int)GamePanel.getYPos(0.70),
                (int)GamePanel.getXPos(0.74),(int)GamePanel.getYPos(0.78));
        this.playButton = new ImageButton(this.playBox,this.image,this.boxes);

    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawText("STACKER",(int)GamePanel.getXPos(0.5),(int)GamePanel.getYPos(0.3),this.text);
        this.playButton.draw(canvas);

    }

    @Override
    public void update() {

    }

    public boolean isPlayPressed(int x,int y){
        return this.playButton.isPressed(x,y);
    }
}
