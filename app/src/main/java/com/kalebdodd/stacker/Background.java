package com.kalebdodd.stacker;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by Kaleb on 8/1/2017.
 */

public class Background implements GameObject {
    private Paint p;

    public Background(){
        p = new Paint();
        p.setColor(Color.BLUE);
        p.setTextSize(70);
    }

    @Override
    public void draw(Canvas canvas) {
		double yOffset = GamePanel.getYOffset();
        double xOffset = GamePanel.getXOffset();
        double lineThickness = GamePanel.getLineThickness();
        double xLength = GamePanel.getXLength();
        double yLength = GamePanel.getYLength();
        double squareLength = GamePanel.getSquareLength();

        Rect backdrop = new Rect((int)xOffset,(int)yOffset,(int)xLength,(int)(yLength+lineThickness));

        Rect topLine = new Rect((int)xOffset,(int)yOffset,(int)xLength,(int)(yOffset+lineThickness));
        Rect bottomLine = new Rect((int)xOffset,(int)yLength,(int)xLength,(int)(yLength+lineThickness));
        Rect leftSideLine = new Rect((int)xOffset,(int)yOffset,(int)(xOffset+lineThickness),(int)(yLength+lineThickness));
        Rect rightSideLine = new Rect((int)(xLength-lineThickness),(int)yOffset,(int)xLength,(int)(yLength+lineThickness));

        p.setColor(Color.WHITE);
        canvas.drawRect(backdrop,p);
        p.setColor(Color.rgb(0,0,255));


        for(int i = 1; i < 7; i++){
            Rect v = new Rect((int)(xOffset + (i*lineThickness) + (i * squareLength)),
                    (int)yOffset,
                    (int)((xOffset + (i*lineThickness) + (i * squareLength)) + lineThickness)
                    ,(int)(yLength+lineThickness)); // vertical lines
            canvas.drawRect(v,p);
        }
        p.setColor(Color.BLUE);

        canvas.drawRect(topLine,p);
        canvas.drawRect(bottomLine,p);
        canvas.drawRect(rightSideLine,p);
        canvas.drawRect(leftSideLine,p);



        for(int i = 1; i < 12; i++){ // 7 x 12 game board
            Rect h = new Rect((int)xOffset,(int)(yOffset + (i*lineThickness) + (i * squareLength)),(int) xLength,
                    (int)((yOffset + lineThickness + (i*lineThickness) + (i*squareLength))));
            canvas.drawRect(h,p);

        }
        //p.setColor(Color.BLUE);
        //canvas.drawText("RES "+(int)GamePanel.getXPos(1.0) +"," + (int)GamePanel.getYPos(1.0),(int)xOffset,(int)(yLength+GamePanel.getYPos(0.075)),p);
		
    }

    @Override
    public void update() {

    }
}
