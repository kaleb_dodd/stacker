package com.kalebdodd.stacker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by balke on 8/30/2017.
 */

public class GameOverMenu implements GameObject {
    private Paint boxes;
    private Paint text;
    private Rect mainMenuBox;
    private ImageButton mainMenuButton;
    private Rect resetBox;
    private ImageButton resetButton;
    private Bitmap menuPic;
    private Bitmap resetPic;


    public GameOverMenu(Context context){
        this.boxes = new Paint();
        this.boxes.setColor(Color.BLUE);
        this.text = new Paint();
        this.text.setColor(Color.rgb(255,165,0));
        this.text.setTextSize(64);
        this.text.setAntiAlias(true);
        this.text.setTextAlign(Paint.Align.CENTER);
        this.menuPic = BitmapFactory.decodeResource(context.getApplicationContext().getResources(), com.kalebdodd.stacker.R.drawable.menu);
        this.resetPic = BitmapFactory.decodeResource(context.getApplicationContext().getResources(), com.kalebdodd.stacker.R.drawable.restart);

        this.mainMenuBox = new Rect((int)GamePanel.getXPos(0.35),(int)GamePanel.getYPos(0.575),
                (int)GamePanel.getXPos(0.65),(int)GamePanel.getYPos(0.635));
        this.resetBox = new Rect((int)GamePanel.getXPos(0.35),(int)GamePanel.getYPos(0.70),
                (int)GamePanel.getXPos(0.65),(int)GamePanel.getYPos(0.76));

        this.resetButton = new ImageButton(this.resetBox,this.resetPic,this.boxes);
        this.mainMenuButton= new ImageButton(this.mainMenuBox,this.menuPic,this.boxes);
    }

    @Override
    public void draw(Canvas canvas) {
        this.resetButton.draw(canvas);
        this.mainMenuButton.draw(canvas);
        canvas.drawText("GAME OVER",(int)GamePanel.getXPos(0.5),(int)GamePanel.getYPos(0.3),this.text);

    }

    @Override
    public void update() {

    }

    public boolean isResetPressed(int x,int y){
        return this.resetButton.isPressed(x,y);
    }

    public boolean isMainMenuPressed(int x, int y){
        return this.mainMenuButton.isPressed(x,y);
    }
}
