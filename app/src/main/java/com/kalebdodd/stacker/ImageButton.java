package com.kalebdodd.stacker;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by Kaleb on 9/5/2017.
 */

public class ImageButton extends Button {
    private Bitmap image;

    public ImageButton(Rect rect, Bitmap image, Paint overlay) {
        super(rect,overlay);
        this.image = image;
    }

    @Override
    public void draw(Canvas canvas){
        canvas.drawBitmap(this.image,null,this.rect,this.boxP);
    }
}
